package elec5619.group9.mentalHealth.repository;

import static org.junit.Assert.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.*;

import elec5619.group9.domain.PersonalInformation;
import elec5619.group9.domain.User;
import elec5619.group9.repository.UserDAO;
import elec5619.group9.service.Register;


@ContextConfiguration(locations = {
		"file:src/main/webapp/WEB-INF/spring/appServlet/persistence-context.xml",
		"file:src/main/webapp/WEB-INF/spring/appServlet/servlet-context.xml", 
		"file:src/main/webapp/WEB-INF/spring/root-context.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@TransactionConfiguration(transactionManager="transactionManager", defaultRollback = true)
public class UserDAOImpTester {
	
	private final Log logger = LogFactory.getLog(getClass());
	
	@Autowired
	UserDAO userdao;
	
	private String email;
	private String name;
	private String password;
	
	
	@Before
	public void beforeTest(){
		email = "xxx@email.com";
		name = "ruishan";
		password = "password";
	}
	
	@Test
	public void addUserTest() {
		User user = new User();
		user.setEmail(email);
		user.setName(name);
		user.setPassword(password);
		
		userdao.addUser(user);
	}
	
	@Test
	public void getUserByEmailTest(){
		User user = userdao.getUserByEmailId(email);
		
		assertEquals(user.getEmail(), email);
	}
	
	@Test
	public void savePersonalInformationTest(){
		User user = userdao.getUserByEmailId(email);
		user.setPersonalInformation(new PersonalInformation());
		PersonalInformation pi = user.getPersonalInformation();
		pi.setAge(10);
		pi.setGender("female");
		pi.setOccupation("tester");
		
		userdao.savePersonalInformation(user, pi);
	}

}
