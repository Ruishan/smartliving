package elec5619.group9.mentalHealth.service;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import elec5619.group9.service.PersonalInformationService;


@ContextConfiguration(locations = {
		"file:src/main/webapp/WEB-INF/spring/appServlet/persistence-context.xml",
		"file:src/main/webapp/WEB-INF/spring/appServlet/servlet-context.xml", 
		"file:src/main/webapp/WEB-INF/spring/root-context.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class PersonalInformationServiceTester {

	@Autowired
	private PersonalInformationService piService;
	
	@Test
	public void isValidAgetest() {
		String age = "somne";
		
		int result = piService.isvalidAge(age);
		
		assertEquals(result, -1);
		
		String age_zero = "0";
		int age_zero_result = piService.isvalidAge(age_zero);
		
		assertEquals(age_zero_result, -1);
		
		String age_null = "";
		int age_null_result = piService.isvalidAge(age_null);
		assertEquals(age_null_result, -1);
		
		String age_int = "23";
		int age_int_result = piService.isvalidAge(age_int);
		assertEquals(age_int_result, 23);
	}
	
	@Test
	public void isValidOccupationTest(){
		String occupation_null = "";
		String null_result = piService.isValidOccupation(occupation_null);
		assertEquals(null_result, "");
		
		String occupation_err = "any123";
		String err_result = piService.isValidOccupation(occupation_err);
		assertEquals(err_result, "0");
		
		String occupation = "student";
		String result = piService.isValidOccupation(occupation);
		assertEquals(occupation, result);
	}

}
