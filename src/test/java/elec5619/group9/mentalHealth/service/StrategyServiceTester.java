package elec5619.group9.mentalHealth.service;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import elec5619.group9.domain.mentalHealth.Strategy;
import elec5619.group9.service.mentalHealth.StrategyService;


@ContextConfiguration(locations = {
		"file:src/main/webapp/WEB-INF/spring/appServlet/persistence-context.xml",
		"file:src/main/webapp/WEB-INF/spring/appServlet/servlet-context.xml", 
		"file:src/main/webapp/WEB-INF/spring/root-context.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class StrategyServiceTester {
	
	@Autowired
	private StrategyService strategyService;

	@Test
	public void restructureStringTest() {
		String test = "   family member               \n" + " is relationship";
		String result = strategyService.restructString(test);
		
		assertEquals(result, "family member is relationship");
	}

}
