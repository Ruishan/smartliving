package elec5619.group9.mentalHealth.service;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import elec5619.group9.service.mentalHealth.StressReasonService;


@ContextConfiguration(locations = {
		"file:src/main/webapp/WEB-INF/spring/appServlet/persistence-context.xml",
		"file:src/main/webapp/WEB-INF/spring/appServlet/servlet-context.xml", 
		"file:src/main/webapp/WEB-INF/spring/root-context.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class StressReasonTester {

	@Autowired
	private StressReasonService srService;
	@Test
	public void isValidCategoryTest() {
		String test = "123";
		
		assertEquals(srService.isValidCategory(test), false);
		
		String test1 = "test1";
		assertEquals(srService.isValidCategory(test1), false);
		
		String test2 = "test";
		assertEquals(srService.isValidCategory(test2), true);
	}

}
