package elec5619.group9.mentalHealth.service;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import elec5619.group9.domain.mentalHealth.PSSResult;
import elec5619.group9.service.mentalHealth.PerformTestService;


@ContextConfiguration(locations = {
		"file:src/main/webapp/WEB-INF/spring/appServlet/persistence-context.xml",
		"file:src/main/webapp/WEB-INF/spring/appServlet/servlet-context.xml", 
		"file:src/main/webapp/WEB-INF/spring/root-context.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class PerformTestServiceTester {
	
	@Autowired
	private PerformTestService ptService;

	@Test
	public void isValidResulttest() {
		PSSResult result = new PSSResult();
		result.setScore(-1);
		
		assertEquals(ptService.isValidResult(result), false);
	}
	
	@Test
	public void selectionToStringTest(){
		String never = "never";
		
		assertEquals(ptService.selectionToScore(never), 0);
		
		String unvalid = "unvalid";
		
		assertEquals(ptService.selectionToScore(unvalid), -1);
	}

}
