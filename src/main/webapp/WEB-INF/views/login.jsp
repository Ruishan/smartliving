<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="true" %>
<%@ include file="/WEB-INF/views/taglib_includes.jsp" %>
<html>
<head>
  <title><spring:message code="app.login"> </spring:message></title>
 

 <script src="js/jquery-1.10.1.js" type="text/javascript"></script>  
  
  
  <script src="https://apis.google.com/js/client:plusone.js"></script>
  
  <script type="text/javascript">
  /*
   * Triggered when the user accepts the sign in, cancels, or closes the
   * authorization dialog.
   */
  function loginFinishedCallback(authResult) {
    if (authResult) {
      if (authResult['error'] == undefined){
        gapi.auth.setToken(authResult); // Store the returned token.
        toggleElement('signin-button'); // Hide the sign-in button after successfully signing in the user.
        getEmail();                     // Trigger request to get the email address.
        getName();
      } else {
        console.log('An error occurred');
      }
    } else {
      console.log('Empty authResult');  // Something went wrong
    }
  }

  /*
   * Initiates the request to the userinfo endpoint to get the user's email
   * address. This function relies on the gapi.auth.setToken containing a valid
   * OAuth access token.
   *
   * When the request completes, the getEmailCallback is triggered and passed
   * the result of the request.
   */
  function getEmail(){
    // Load the oauth2 libraries to enable the userinfo methods.
    gapi.client.load('oauth2', 'v2', function() {
          var request = gapi.client.oauth2.userinfo.get();
          request.execute(getEmailCallback);
        });
  }

  function getEmailCallback(obj){
    var el = document.getElementById('email');
    var email = '';
    var email1='';

    if (obj['email']) {
      email = 'Email: ' + obj['email'];
      email1=obj['email'];
    }

    //console.log(obj);   // Uncomment to inspect the full object.

    el.innerHTML = email;
    toggleElement('email');
    //call ajax
    doAjaxPost(email1)
  }
  
  function getName(){
  	gapi.client.load('plus','v1', function(){
	 var request = gapi.client.plus.people.get({
	   'userId': 'me'
	 });
	 request.execute(getNameCallback);
	});
	
  }
  
  function getNameCallback(obj){
  	var el = document.getElementById('name');
    var name = '';

    if (obj['displayName']) {
      name = 'Name: ' + obj['displayName'];
    }
    
    console.log('Retrieved profile for:' + obj['displayName']);

    //console.log(obj);   // Uncomment to inspect the full object.

    el.innerHTML = name;
    toggleElement('name');
  }

  function toggleElement(id) {
    var el = document.getElementById(id);
    if (el.getAttribute('class') == 'hide') {
      el.setAttribute('class', 'show');
    } else {
      el.setAttribute('class', 'hide');
    }
  }
  </script>
</head>
<body>

<!-- AJAX function: sending data back to server -->

  <script>  
   function doAjaxPost(email) {  
      
    window.location.href = "./validateEmail?email="+email;  
//     $.ajax({  
//      type : "Get",   
//      url : "validateEmail",   
//      data : "email=" +emailT,  
//      success : function(response) {  
//     	 window.location.href = "http://localhost:8080/mentalhealth/";   
//      },  
//      error : function(e) {  
//       alert('Error: ' + e);   
//      }  
//      });  
   }  
  </script> 



<!-- End AJAX function: sending data back to server -->


<div id="container" style="width:1024px">

<!-- Header -->
<div id="header" style="background-color:#FFA500;border-width:0px;margin:0px;">
<<<<<<< HEAD


<%@include file="headerP.jsp" %>
=======
<<<<<<< HEAD


<%@include file="headerP.jsp" %>
=======
<<<<<<< HEAD


<%@include file="headerP.jsp" %>
</div>
<!-- Sub Header -->
<div id="sub_header" style="background-color:#a8d4e5;border-width:0px;margin:0px;text-align:left;width:1024px; ">
<a href="./" title="home page">Home&nbsp;&nbsp;</a>

=======


<%@include file="header.jsp" %>
>>>>>>> 8aa4961b5a7289c302b67b5aa77f373d5a043285
>>>>>>> 906a8e062214c1fbe50490ea2ce410000de0664d
</div>
<!-- Sub Header -->
<div id="sub_header" style="background-color:#a8d4e5;border-width:0px;margin:0px;text-align:left;width:1024px; ">
<a href="./" title="home page">Home&nbsp;&nbsp;</a>

<<<<<<< HEAD
=======
<<<<<<< HEAD
=======
>>>>>>> 08bb5628c58809b26a98454c994d64a267ec564f
>>>>>>> 8aa4961b5a7289c302b67b5aa77f373d5a043285
>>>>>>> 906a8e062214c1fbe50490ea2ce410000de0664d
</div>

<!-- Menu -->
<div id="menu" style="background-color:#e0e6e6;height:400px;width:200px;float:left;">

</div>

<!-- Content -->
<div id="content" style="background-color:#e0e6e6;height:400px;width:822px;float:left;">

<!-- Main Login Code -->

  <div id="signin-button" class="show">
     <div class="g-signin" data-callback="loginFinishedCallback"
      data-approvalprompt="force"
      data-clientid="688721127022-j9c74jkphs496bqjbm6i7j55como9u1r.apps.googleusercontent.com"
      data-scope="https://www.googleapis.com/auth/userinfo.email
      			  https://www.googleapis.com/auth/plus.me"
      data-height="short"
      data-cookiepolicy="single_host_origin"
      >
    </div>
    <!-- In most cases, you don't want to use approvalprompt=force. Specified
    here to facilitate the demo.-->
  </div>

  <div id="email" class="hide"></div>
  <div id="name" class="hide"></div>
  <h3>login with username and password or using Google sign in:</h3>
  
		
<form action="login" method="POST">



<table id="one-column-emphasis" summary="2007 Major IT Companies' Profit">
    <colgroup>
    	<col class="oce-first" />
    </colgroup>
    <thead>
    	<tr>
        	<th scope="col"></th>
            <th scope="col"></th>
    
        </tr>
    </thead>
    <tbody>
    	<tr>
        	<td>User Email:</td>
            <td><input type="text" name="email">
		<p style="color:red;">${error} </p></td>

        </tr>
        <tr>
        	<td>Password</td>
            <td><input type="password" name="password"></td>
   
        </tr>
        <tr>
        <td></td>
        <td><input type="submit" value="Login" />
		<p style="color:red;">${message}</p>
	</form></td>
        </tr>
      <tr>
      <td>
      </td>
      <td>
        <h3> <a href="./register">Register new user</a></h3>
      </td>
      </tr>
    </tbody>
</table>

	


<!--  End Main Login Code -->
 
</div>

<!-- Footer -->
<div id="footer" style="background-color:#FFA500;clear:both;text-align:center;">

<%@include file="footer.jsp" %>
</div>

</div>





</body>
</html>



