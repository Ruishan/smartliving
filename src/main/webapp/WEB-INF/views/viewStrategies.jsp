<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page import="java.util.List" %>
<%@ page session="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

	<head>
	
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title><spring:message code="app.title"></spring:message></title>
	
	</head>
	
	<body>
	
		<% Object user = session.getAttribute("user");%>
		
<div id="container" style="width:1024px">

<!-- Header -->
<div id="header" style="background-color:#FFA500;border-width:0px;margin:0px;">
<h1 style="margin-bottom:0;">

<%@include file="header.jsp" %>
</h1></div>
<!-- Sub Header -->
<div id="sub_header" style="background-color:#a8d4e5;border-width:0px;margin:0px;text-align:right; ">

<a href="login">Log Out</a>&nbsp;&nbsp;&nbsp;<a href="./editPersonalInformation">Edit Personal Information&nbsp;&nbsp;</a>

</div>

<!-- Menu -->
<div id="menu" style="background-color:#e0e6e6;height:400px;width:200px;float:left;">

<%@include file="menuSecure.jsp" %>
</div>

<!-- Content -->
<div id="content" style="background-color:#e0e6e6;height:400px;width:822px;float:left;">
		
		<form action="addStrategy" method="POST">
			<table border="1">
				<tr>
					<th>Stress</td>
					<th>Strategy to deal with</th>
				</tr>
				
				<c:forEach items="${strategies}" var="strategy">
					<tr>
						<td><c:out value="${strategy.reason}"/> </td>
						<td><c:out value="${strategy.strategy}"/> </td>
					</tr>
				</c:forEach>
				
			</table>
		
		</form>
		
		</div>
		
		<!-- Footer -->
		<div id="footer" style="background-color:#FFA500;clear:both;text-align:center;width:1024px;">
		
			<%@include file="footer.jsp" %>
		</div>
	</body>
</html>