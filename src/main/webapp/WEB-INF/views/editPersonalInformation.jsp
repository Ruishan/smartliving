<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page session="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

	<head>
	
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title><spring:message code="app.title"></spring:message></title>
	
	</head>
	
	<body>
		<% Object user = session.getAttribute("user");%>
		
<div id="container" style="width:1024px">

<!-- Header -->
<div id="header" style="background-color:#FFA500;border-width:0px;margin:0px;">
<h1 style="margin-bottom:0;">

<%@include file="header.jsp" %>
</h1></div>
<!-- Sub Header -->
<div id="sub_header" style="background-color:#a8d4e5;border-width:0px;margin:0px;text-align:right; ">

<a href="login">Log Out</a>&nbsp;&nbsp;&nbsp;<a href="./editPersonalInformation">Edit Personal Information&nbsp;&nbsp;</a>

</div>

<!-- Menu -->
<div id="menu" style="background-color:#e0e6e6;height:400px;width:200px;float:left;">

<%@include file="menuSecure.jsp" %>
</div>

<!-- Content -->
<div id="content" style="background-color:#e0e6e6;height:400px;width:822px;float:left;">
		
		<h2>Keep Personal Information up to date is help to keep tract of your health!</h2>
		<form action="editPersonalInformation" method="POST">
			
			<table>
				<tr>
					<td>
						Age: <input type="text" name="age" value=${user.personalInformation.age}> </input>
					</td>
					<td>
						<p style="color:red;">${invalid_age}</p>
					</td>
				</tr>
				
				<tr>
					<td>
						Gender: Female<input type="radio" name="gender" value="female"
						<c:if test="${female_check == 'check'}">CHECKED</c:if> /> 
						Male <input type="radio" name="gender" value="male"
						<c:if test="${male_check == 'check'}">CHECKED</c:if> />
					</td>
				</tr>
				
				<tr>
					<td>
						Marriage: Yes<input type="radio" name="marriage" value="yes"
							<c:if test="${marriage_yes == 'yes'}">CHECKED</c:if> />
						No<input type="radio" name="marriage" value="no" 
							<c:if test="${marriage_no == 'no'}">CHECKED</c:if> />
					</td>
				</tr>
				
				<tr>
					<td>
						Occupation: <input type="text" name="occupation" value=${user.personalInformation.occupation}> </input>
					</td>
					<td>
						<p style="color:red;">${invalid_occupation}</p>
					</td>
				</tr>
				
				<tr>
					<td>
						<p>${save_successful}</p>
					</td>
				</tr>
				
				<tr>
					<td>
						<input type="submit" value="Save" />
					</td>
				</tr>
					
			<table>
		</form>
		
		</div>
		
		<!-- Footer -->
		<div id="footer" style="background-color:#FFA500;clear:both;text-align:center;">
		
			<%@include file="footer.jsp" %>
		</div>
	</body>
</html>