<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/views/taglib_includes.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><spring:message code="app.title"></spring:message></title>

</head>
<body>

<body>

<div id="container" style="width:1024px">

<!-- Header -->
<div id="header" style="background-color:#FFA500;border-width:0px;margin:0px;">
<h1 style="margin-bottom:0;">

<%@include file="headerP.jsp" %>
</h1></div>
<!-- Sub Header -->
<div id="sub_header" style="background-color:#a8d4e5;border-width:0px;margin:0px;text-align:right;width:1024px; ">

<a href="login">Log In</a>&nbsp;&nbsp;&nbsp;<a href="./register">Register new user</a>

</div>

 <!-- Menu --> 
<div id="menu" style="background-color:#e0e6e6;height:650px;width:224px;float:left;">
<table>
<tr>
<td>
<img src="images/smoke01.jpg" />
</td>

</tr>

<tr>
<td><img src="images/mental.jpg" /></td>
</tr>
<tr>
<td><img src="images/bp.jpg" /></td>
</tr>
</table>

</div>

<!-- Content -->
<div id="content" style="background-color:#e0e6e6;height:650px;width:800px;float:left;">

<h2>Welcome to home of Smartliving.</h2> 


<p>IT technology can be used to leverage behavior change, creating awareness, and even
improving or preventing health problems. In addition, they can spread this concept to millions of people via Internet. Social media in
the Internet influences billions of people around the world.</p>
<p>The purpose of this project is to record and track personal health data in the daily life in order to improve the quality of life.
  In the modern society, the life pace becomes more and more fast and some unhealthy habits are accompanied. 
  As a result, the health issues are the major concern by the community. Our project mainly focuses on four areas which
  are snoring in the sleep, smoking issue, hypertension concern and depression and mental health.
  <h2>Aims of the Project</h2>
  <ul>
  <li>Increase awareness to 4 lifelong diseases that threaten people by developing web
application that can be a central place for people to access knowledge and monitor
their health.</li>
  <li>Let each individual to record health status and can track their health’s records
through graphical data or tabular form. Furthermore, integrating with Google Sign in
will spread the awareness of the wellbeing
into a much larger audience.</li>
  <li>Develop the web application by using reusable technology such as framework to
reduce developing time and increase collaboration among developers.</li>
  
  </ul>
  
<p>For more information, please visit our home page at <a href="https://bitbucket.org/Ruishan/smartliving/wiki/Home">SmartLiving</a> home.</p>




</div>

<!-- Footer -->
<div id="footer" style="background-color:#FFA500;clear:both;text-align:center;width:1024px;">

<%@include file="footer.jsp" %>
</div>

</div>


</body>
</html>