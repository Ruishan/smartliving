<%@ include file="/WEB-INF/views/taglib_includes.jsp" %>
<%@ page session="true" %>

<html>
<head>
	
<!--------------------    JQuery for DateTime Picker    ---------------------->
	  <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />

  <script src="http://code.jquery.com/jquery-1.9.1.js"></script>

  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

  <link rel="stylesheet" href="/resources/demos/style.css" />

  <script>

  $(function() {
    $( "#datepicker" ).datepicker();

  });

  </script>
	
<!---------------------- End JQuery DateTime Picker ------------------------->
 
<style type="text/css">
 .form_error { 
 color: #ff0000; 
 font-weight: bold; 
 } 
</style>

<title><spring:message code="app.title"></spring:message> </title>
</head>
<body >
<% Object user = session.getAttribute("user");
%>

<div id="container" style="width:1024px">

<!-- Header -->
<div id="header" style="background-color:#FFA500;border-width:0px;margin:0px;">
<h1 style="margin-bottom:0;">

<%@include file="header.jsp" %>
</h1></div>
<!-- Sub Header -->
<div id="sub_header" style="background-color:#a8d4e5;border-width:0px;margin:0px;text-align:right; ">

<a href="login">Log Out</a>&nbsp;&nbsp;&nbsp;<a href="./editPersonalInformation">Edit Personal Information&nbsp;&nbsp;</a>

</div>

<!-- Menu -->
<div id="menu" style="background-color:#e0e6e6;height:400px;width:200px;float:left;">

<%@include file="menuSecure.jsp" %>
</div>

<!-- Content -->
<div id="content" style="background-color:#e0e6e6;height:400px;width:822px;float:left;">

<table  >
	<tr>
		<td align="center"><h3>Add Hypertension Form</h3></td>
	</tr>
	<tr valign="top" align="center">
    <td align="center">
 		<form:form action="addHypertension" method="post" modelAttribute="bloodPressure">
	    	
				<table >	
					<tr>
						<td >Description</td>
						<td ><form:input path="description"/></td>
						<td>
						<form:errors path="description" cssClass="form_erro" /> 
						</td>
					</tr>
					<tr>
						<td >Systolic reading (top reading)</td>
						<td ><form:input path="systolic"/></td>
						<td>
						<form:errors path="systolic" cssClass="form_error"/> 
						</td>
					</tr>
					
					<tr>
					<tr>
						<td >Diastolic reading (buttom reading)</td>
						<td ><form:input path="diastolic"/></td>
						<td>
						<form:errors path="diastolic" cssClass="form_error"/> 
						</td>
					</tr>
					<tr>
						<td >Heart rate</td>
						<td ><form:input  path="heartRate"/></td>
						<td>
						<form:errors path="heartRate" cssClass="form_error"/> 
						</td>
					</tr>

					<tr>
						<td >Recorded Date</td>
						
						<td ><form:input  id="datepicker" path="recordedDate"/></td>
						<td>
						<form:errors path="recordedDate" cssClass="form_error"/> 
						</td>
					</tr>
<!-- 						<tr> -->
<!-- 						<td></td> -->
<%-- 						<td><input type="hidden" name="email" path="${} }"/> --%>
<!-- 						</td> -->
<!-- 						</tr>				 -->
<!-- 					<tr> -->
						<td >
						<input type="submit" name="" value="Save">
						&nbsp;&nbsp;
						<input type="reset" name="" value="Reset">
						</td>
					</tr>					
				</table>			
		</form:form>
    </td>    
  </tr>
</table>

</div>

<!-- Footer -->
<div id="footer" style="background-color:#FFA500;clear:both;text-align:center;">

<%@include file="footer.jsp" %>
</div>

</div>



</body>
</html>




















</body>
</html>
