<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/views/taglib_includes.jsp" %>
<%@ page session="true" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><spring:message code="app.title"></spring:message></title>

</head>
<body>

<body>
<% Object user = session.getAttribute("user");%>
<div id="container" style="width:1024px">

<!-- Header -->
<div id="header" style="background-color:#FFA500;border-width:0px;margin:0px;">
<h1 style="margin-bottom:0;">

<%@include file="header.jsp" %>
</h1></div>
<!-- Sub Header -->
<div id="sub_header" style="background-color:#a8d4e5;border-width:0px;margin:0px;text-align:right; ">

<a href="login">Log Out</a>&nbsp;&nbsp;&nbsp;<a href="./editPersonalInformation">Edit Personal Information</a>&nbsp;&nbsp;

</div>

<!-- Menu -->
<div id="menu" style="background-color:#e0e6e6;height:400px;width:200px;float:left;">

<%@include file="menuSecure.jsp" %>
</div>

<!-- Content -->
<div id="content" style="background-color:#e0e6e6;height:400px;width:822px;float:left;">

<h2>Welcome to smart monitoring you health</h2>
<p> we have tools to help you monitoring your daily health. You can go to each menu and get the 
tool you need.  We have the tools such as:

<p>Blood pressure monitoring: this is a great way to keep record of your blood pressure and be able
 to review the result later. You can track the level of your blood pressure as well.
 </p>
 <p>Mental health monitoring: anxiety is part of our life.  Our tools offer a chance to record your
 stress reason, set up your goal, and answering some of the mental health related question.
  </p>
<p>Smoking: you can set and record your goal to reduce smoking habit.</p>
  


</div>

<!-- Footer -->
<div id="footer" style="background-color:#FFA500;clear:both;text-align:center;width:1024px;">

<%@include file="footer.jsp" %>
</div>

</div>


</body>
</html>