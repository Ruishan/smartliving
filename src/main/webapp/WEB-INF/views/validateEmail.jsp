<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/views/taglib_includes.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><spring:message code="app.title"></spring:message></title>

</head>
<body>

<body>

<div id="container" style="width:1024px">

<!-- Header -->
<div id="header" style="background-color:#FFA500;border-width:0px;margin:0px;">
<h1 style="margin-bottom:0;">

<%@include file="header.jsp" %>
</h1></div>
<!-- Sub Header -->
<div id="sub_header" style="background-color:#a8d4e5;border-width:0px;margin:0px;text-align:right; ">

<a href="login">Log Out</a>&nbsp;&nbsp;&nbsp;<a href="./editPersonalInformation">Edit Personal Information&nbsp;&nbsp;</a>

</div>

<!-- Menu -->
<div id="menu" style="background-color:#e0e6e6;height:400px;width:200px;float:left;">

<%@include file="menuSecure.jsp" %>
</div>

<!-- Content -->
<div id="content" style="background-color:#e0e6e6;height:400px;width:822px;float:left;">

  



	<form action="./validateEmail" method="POST">
	
	  <table>
	  <tr>
	  <td>User email:</td>
	  <td><input type="text" name="email">
		<p>${error} </p></td>
	  </tr>
 	  
	  <tr>
	  <td></td><td><input type="submit" value="Submit" /></td>
	  </tr>
	  </table>
	  		
	</form>










</div>

<!-- Footer -->
<div id="footer" style="background-color:#FFA500;clear:both;text-align:center;">

<%@include file="footer.jsp" %>
</div>

</div>


</body>
</html>