<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/views/taglib_includes.jsp" %>
<%@ page session="true" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><spring:message code="app.title"></spring:message></title>

</head>
<body>

<body>

<div id="container" style="width:1024px">

<!-- Header -->
<div id="header" style="background-color:#FFA500;border-width:0px;margin:0px;">
<h1 style="margin-bottom:0;">

<%@include file="../header.jsp" %>
</h1></div>
<!-- Sub Header -->
<div id="sub_header" style="background-color:#a8d4e5;border-width:0px;margin:0px; ">
<h3 style="margin-bottom:0;text-align:right; margin-right:10px;">
<a href="login">Log In</a>&nbsp;&nbsp;&nbsp;<a href="./register">Register new user</a>

</h3></div>

<!-- Menu -->
<div id="menu" style="background-color:#FFFFCC;height:400px;width:200px;float:left;">

<%@include file="../menuSecure.jsp" %>
</div>

<!-- Content -->
<div id="content" style="background-color:#EEEEEE;height:400px;width:822px;float:left;">




	
	<table style="border-collapse: collapse;" border="1" bordercolor="#006699" width="500">
		<tr bgcolor="lightblue">
			<th>Id</th>
			<th>Description</th>			
			<th>Systolic</th>	
			<th>Diastolic</th>
			<th>Heart Rate</th>
            <th>Reocorded Date</th>
            <th></th>
            
		</tr>
		<c:if test="${empty HYPERTENSION_LIST}">
		<tr>
			<td colspan="4">No Results found</td>
		</tr>
		</c:if>
		<c:if test="${! empty HYPERTENSION_LIST}">
			<c:forEach var="hypertension" items="${HYPERTENSION_LIST}">		
		    <tr>
				<td><c:out value="${hypertension.id}"></c:out></td>
				<td><c:out value="${hypertension.description}"></c:out></td>
				<td><c:out value="${hypertension.systolic}"></c:out> </td>
				<td><c:out value="${hypertension.diastolic}"></c:out></td>
				<td><c:out value="${hypertension.heartRate}"></c:out> </td>
				<td><c:out value="${hypertension.RerocdedDate}"></c:out></td>

				<td>
					&nbsp;<a href="/editHypertension?id=${hypertension.id}">Edit</a>
					&nbsp;&nbsp;<a href="/deleteHypertension?id=${hypertension.id}">Delete</a>
				</td>
			</tr>
			</c:forEach>
		</c:if>				
	</table>	
	










  
</div>

<!-- Footer -->
<div id="footer" style="background-color:#FFA500;clear:both;text-align:center;">

<%@include file="../footer.jsp" %>
</div>

</div>


</body>
</html>