package elec5619.group9.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.MapKey;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.validation.constraints.NotNull;

import elec5619.group9.domain.mentalHealth.PSSResult;
import elec5619.group9.domain.mentalHealth.StressReason;

@Entity
//@Table(name="User")
public class User implements Serializable{
	
	@Id
	private String email;
	
	private String password;
	
	@NotNull
	private String name;
	
	@NotNull
	@Column(name="isGoogle")
	private boolean isGoogle;
	
	@OneToOne(cascade = CascadeType.ALL)
	private PersonalInformation personalInformation;
	
	
	@OneToMany(mappedBy="user",fetch = FetchType.EAGER)
	@MapKey
	private Set<PSSResult> results = new HashSet<PSSResult>();
	
	@OneToMany(mappedBy="user", fetch = FetchType.EAGER)
	@MapKey
	private Set<StressReason> stressReasons = new HashSet<StressReason>();

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public PersonalInformation getPersonalInformation() {
		
		if (personalInformation == null){
			this.personalInformation = new PersonalInformation();
		}
		this.personalInformation.setEmail(email);
		return this.personalInformation;
	}

	public void setPersonalInformation(PersonalInformation personalInformation) {
		this.personalInformation = personalInformation;
	}

	public Set<PSSResult> getResults() {
		if(this.results == null)
			this.results = new HashSet<PSSResult>();
		return results;
	}

	public void setResults(HashSet<PSSResult> results) {
		this.results = results;
	}

	public Set<StressReason> getStressReasons() {
		if(this.stressReasons == null)
			this.stressReasons = new HashSet<StressReason>();
		return stressReasons;
	}

	public void setStressReasons(Set<StressReason> stressReasons) {
		this.stressReasons = stressReasons;
	}

	public boolean getIsGoogle() {
		return isGoogle;
	}

	public void setIsGoogle(boolean isGoogle) {
		this.isGoogle = isGoogle;
	}
}
