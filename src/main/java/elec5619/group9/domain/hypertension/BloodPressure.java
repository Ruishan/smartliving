package elec5619.group9.domain.hypertension;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

import elec5619.group9.domain.PersonalInformation;


@Entity
@Table(name="BloodPressure")
public class BloodPressure implements Serializable {
	
	private static final long serialVersionUID = 1L;
	

	@Id
	@GeneratedValue
	@Column(name="Id")
	private long id;
    

	@NotEmpty
    @Column(name="description")
    private String description;
    
	@NotNull(message="Can not be null")
    @Range(min = 1, max = 500)
	@NumberFormat(style = Style.NUMBER) 
    @Column(name="systolic")
    private Integer systolic;
    
	@NotNull(message="Can not be null")
	@Range(min = 1, max = 500)
	@NumberFormat(style = Style.NUMBER)
    @Column(name="diastolic")
    private Integer diastolic;
    
	@NotNull(message="Can not be null")
	@Range(min = 1, max = 500)
	@NumberFormat(style = Style.NUMBER)
    @Column(name="heartRate")
    private Integer heartRate;
    
	//@NotNull(message="Can not be empty.")
	//@DateTimeFormat(pattern="dd/MM/yyyy")
    @Column(name="recordedDate")
    private Date recordedDate;
	
    @Column(name="email")
    private String email;
	
    @OneToOne(cascade = CascadeType.ALL)
	private HealthStatus healthStatus;
		
/* ************************************************************
 * getter and setter
 */
    public long getId() {
        return this.id;
    }
    public void setId(long Id) {
        this.id = Id;
    }
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    
    public Integer getSystolic() {
        return this.systolic;
    }
    
    public void setSystolic(Integer systolic) {
        this.systolic = systolic;
    }
    
    public Integer getDiastolic() {
        return this.diastolic;
    }
    
    public void setDiastolic(Integer diastolic) {
        this.diastolic = diastolic;
    }
    
    public Integer getHeartRate() {
        return this.heartRate;
    }
    
    public void setHeartRate(Integer heartRate) {
        this.heartRate = heartRate;
    }
    
    public Date getRecordedDate() {
        return this.recordedDate;
    }
    
    public void setRecordedDate(Date recordedDate) {
        this.recordedDate = recordedDate;
    }
    
    public String getEmail() {
        return this.email;
    }
    
        
    public void setEmail(String email) {
        this.email = email;
    }
        
 public HealthStatus getHealthStatus() {
		
		if (this.healthStatus == null){
			this.healthStatus = new HealthStatus();
		}
		this.healthStatus.setEmail(email);
		return this.healthStatus;
	}

	public void setHealthStatus(HealthStatus healthStatus) {
		this.healthStatus = healthStatus;
	}
		
	
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 * override toString() method
	 */
	public String toString(){
		StringBuffer buffer = new StringBuffer();
		buffer.append(" ");
		return buffer.toString();
	}

}
