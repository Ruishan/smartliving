package elec5619.group9.domain.mentalHealth;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.ForeignKey;

import elec5619.group9.domain.User;

@Entity
public class StressReason implements Serializable{
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int stressId;

	/**
	@NotNull
	@ForeignKey(name = "email")
	private String email;
	*/
	
	@ManyToOne
	private User user;
	
	@NotNull
	private Date stressTime;
	
	@NotNull
	private String reason;
	
	@NotNull
	private String category;

	public Date getStressTime() {
		return stressTime;
	}

	public void setStressTime(Date stressTime) {
		this.stressTime = stressTime;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	/**
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	*/

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public int getStressId() {
		return stressId;
	}

	public void setStressId(int stressId) {
		this.stressId = stressId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
