package elec5619.group9.domain.mentalHealth;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class Strategy {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int commentId;
	
	@NotNull
	@Column(length=10000000)
	private String reason;
	
	@NotNull 
	private String strategy;
	
	public void setCommentId(int commentId){
		this.commentId = commentId;
	}

	public int getCommentId() {
		return commentId;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getStrategy() {
		return strategy;
	}

	public void setStrategy(String strategy) {
		this.strategy = strategy;
	}
}
