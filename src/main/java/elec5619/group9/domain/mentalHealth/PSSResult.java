package elec5619.group9.domain.mentalHealth;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import elec5619.group9.domain.User;

@Entity
public class PSSResult implements Serializable{
	/**
	@Id
	@NotNull
	private String email;
	*/
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int resultId;
	
	@ManyToOne
	private User user;
	
	@NotNull
	private Date takenTime;
	
	@NotNull
	private int score;

	public Date getTakenTime() {
		return this.takenTime;
	}

	public void setTime(Date takenTime) {
		this.takenTime = takenTime;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public int getResultId() {
		return resultId;
	}

	public void setResultId(int resultId) {
		this.resultId = resultId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	/**
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	*/
	
	

}
