package elec5619.group9.service.mentalHealth;

import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import elec5619.group9.domain.User;
import elec5619.group9.domain.mentalHealth.Strategy;
import elec5619.group9.domain.mentalHealth.StressReason;
import elec5619.group9.repository.UserDAO;
import elec5619.group9.repository.mentalHealth.MenatalHealthDAO;

@Service
public class StrategyService {
	
	@Autowired
	private UserDAO userDao;
	
	@Autowired
	private MenatalHealthDAO mentalHealthDao;
	
	public void addStrategy(Strategy strategy){
		mentalHealthDao.addStratey(strategy);
	}
	
	public String restructString(String str){
		String output = "";
		Scanner scn = new Scanner(str);
		while(scn.hasNext()){
			output += scn.next() + " ";
		}
		return output.trim();
	}
	
	public StressReason getStressReasonById(int id){
		return mentalHealthDao.getStressReasonById(id);
	}
	
	public List<Strategy> allStrategies(){
		return mentalHealthDao.allStrateties();
	}

}
