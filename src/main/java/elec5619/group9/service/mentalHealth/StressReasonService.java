package elec5619.group9.service.mentalHealth;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import elec5619.group9.domain.User;
import elec5619.group9.domain.mentalHealth.StressReason;
import elec5619.group9.repository.UserDAO;

@Service(value="stressReasonSerive")
public class StressReasonService {
	
	@Autowired
	private UserDAO userDao;
	
	public boolean isValidCategory(String category){
		for(int i = 0; i < category.length(); i++){
			if(!Character.isLetter(category.charAt(i)))
				return false;
		}
		return true;
	}
	
	public void addStressReason(User user, StressReason reason){
		reason.setUser(user);
		reason.setStressTime(new Date());
		userDao.addStressReasonFor(user, reason);
	}
	
	public String restructString(String input){
		String output = "";
		Scanner scn = new Scanner(input);
		while(scn.hasNext()){
			output += scn.next() + " ";
		}
		return output.trim();
	}

}
