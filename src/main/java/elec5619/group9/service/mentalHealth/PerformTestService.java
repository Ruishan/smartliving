package elec5619.group9.service.mentalHealth;

import java.util.ArrayList;
import java.util.Date;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import elec5619.group9.domain.User;
import elec5619.group9.domain.mentalHealth.PSSResult;
import elec5619.group9.repository.UserDAO;

@Service(value="performTestService")
public class PerformTestService {
	
	@Autowired
	private UserDAO userDao;
	
	public boolean addResultFor(User user, PSSResult result){
		if(result != null && user != null){
			if(isValidResult(result)){
				result.setUser(user);
				userDao.addPSSResultFor(user, result);
				return true;
			}
		}
		return false;
	}
	
	public boolean isValidResult(PSSResult result){
		if(result.getScore() >= 0)
			return true;
		return false;
	}
	
	public String stressLevel(int score){
		if(score >= 0 && score <= 7)
			return "Much Lower than Average";
		else if(score >= 8 && score <= 11)
			return "Slightly Lower than Average";
		else if(score >= 12 && score <= 15)
			return "Average";
		else if (score >= 16 && score <= 20)
			return "Slightly Higher than Average";
		else
			return "Much Higher than Average";
	}
	
	public int selectionToScore(String selection){
		if(selection != null){
			if(selection.equalsIgnoreCase("never"))
				return 0;
			else if (selection.equalsIgnoreCase("anever"))
				return 1;
			else if(selection.equalsIgnoreCase("sometimes"))
				return 2;
			else if(selection.equalsIgnoreCase("foften"))
				return 3;
			else if (selection.equalsIgnoreCase("voften"))
				return 5;
		}
		return -1;
	}
}
