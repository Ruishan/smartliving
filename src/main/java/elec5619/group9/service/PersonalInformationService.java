package elec5619.group9.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import elec5619.group9.domain.PersonalInformation;
import elec5619.group9.domain.User;
import elec5619.group9.repository.UserDAO;

@Service(value="personalInformationService")
public class PersonalInformationService {
	
	@Autowired
	private UserDAO userDao;
	
	public boolean savePersonalInformationFor(User user, PersonalInformation pi){
		if(user != null){
			userDao.savePersonalInformation(user, pi);
			return true;
		}
		return false;
	}
	
	public int isvalidAge(String input){
		if(input != null){
			int age;
			try{
				
				age = Integer.parseInt(input);
				if((age > 0 && age <= 120))
					return age;
				else return -1;
			}catch (Exception e){
				return -1;
			}
		}
		else return 0;
		
	}
	
	public String isValidOccupation(String input){
		if(input != null){
			for(int i = 0; i < input.length(); i++){
				if(!Character.isLetter(input.charAt(i)))
					return "0";
			}
			return input;
		}
		else return "";
	}
	
	/**
	 * convert string marriage to boolean value
	 * @param marriage
	 * @return
	 */
	public boolean marriageToBoolean(String marriage){
		if(marriage != null){
			if(marriage.equalsIgnoreCase("yes"))
				return true;
			else
				return false;
		} else return false;
	}
}
