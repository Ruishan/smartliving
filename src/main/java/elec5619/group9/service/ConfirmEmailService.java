package elec5619.group9.service;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;  
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;  
import org.springframework.web.bind.annotation.RequestParam;    
import org.springframework.web.bind.annotation.SessionAttributes;

import elec5619.group9.domain.PersonalInformation;
import elec5619.group9.domain.User;
import elec5619.group9.repository.UserDAO;

  
@Controller(value="confirmEmailService")
@SessionAttributes(value="user",types=User.class)
public class ConfirmEmailService {  
  
	@Resource(name="login")
	private Login login;
	
		
	@Autowired
	private PersonalInformationService piService;
	@Autowired
	private UserDAO userDao;
	

// @RequestMapping("/validateEmail")  
// public @ResponseBody  
// String hello(@RequestParam(value = "email") String email) {  
//  System.out.println(email);  
//  String  aa="asdfas";
// return "newUser";  
//  
// }  

 
 @RequestMapping("/validateEmail")  
 String validateGoogleUser(@RequestParam(value = "email") String email,ModelMap model) {  
  System.out.println(email);  
  //check if user is in the system
  User user = new User();
  user.setEmail(email);
	User validatedUser = login.isValidGoogleUser(user);
	if(validatedUser != null  ){//Exist in database
		model.addAttribute("user", validatedUser);
	}
	else{ //Not in the database
		user.setName("GoogleUser");
		user.setIsGoogle(true);
		//Save into the database
		userDao.addUser(user);
		PersonalInformation pi = new PersonalInformation();
		piService.savePersonalInformationFor(user, pi);
		model.addAttribute("user", user);
	}
  
	return "redirect:/HomeSecured";
  
 }  
 
}  

