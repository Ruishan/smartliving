package elec5619.group9.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import elec5619.group9.domain.User;
import elec5619.group9.repository.UserDAO;

@Service(value="register")
public class Register {
	
	@Autowired
	private UserDAO userDAO;
	
	
	public void registerUser(User user){
		userDAO.addUser(user);
	}
	
	public  boolean isExistUser(String email){
		User user = userDAO.getUserByEmailId(email);
		if(user == null)
			return false;
		return true;
	}
	
	public boolean isValidEmailFormate(String email){
		String spaceSplite [] = email.split(" ");
		
		if(spaceSplite.length != 1 || spaceSplite.length == 0)
			return false;
		
		if(email.charAt(0) == '@' || email.charAt(email.length() - 1) == '@')
			return false;
		
		return true;
	}

	public UserDAO getUserDAO() {
		return userDAO;
	}

	public void setUserDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
	}

}
