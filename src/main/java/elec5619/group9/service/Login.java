package elec5619.group9.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import elec5619.group9.domain.User;
import elec5619.group9.repository.UserDAO;

@Service(value="login")
public class Login {
	
	@Autowired
	private UserDAO userdao;
	
	public User isValidUser(User user) {
		User u = userdao.getUserByEmailId(user.getEmail());
		if(u != null){
			if(u.getPassword().equals(user.getPassword()))	
				return u;
		}
		return null;
	}
	
	public User isValidGoogleUser(User user) {
		User u = userdao.getUserByEmailId(user.getEmail());
		if(u != null){
				return u;
		}
		return null;
	}

}
