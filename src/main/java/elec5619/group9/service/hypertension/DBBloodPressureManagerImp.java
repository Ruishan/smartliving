package elec5619.group9.service.hypertension;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import elec5619.group9.domain.hypertension.BloodPressure;
import elec5619.group9.domain.hypertension.HealthStatus;

@Service(value="bloodPressureManager")
@Transactional
public class DBBloodPressureManagerImp implements BloodPressureManager{
	
	
	private static final long serialVersionUID = 1L;
	//Hibernate
	private SessionFactory sessionFactory;
	
	@Autowired
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<BloodPressure> getBloodPressures(){
		//TODD
		return this.sessionFactory.getCurrentSession().createSQLQuery("From BloodPressure").list();
	}
	
	

	@Override
	public void addBloodPressure(BloodPressure bloodPressure,HealthStatus healthStatus) {
		// TODO Auto-generated method stub
		this.sessionFactory.getCurrentSession().save(bloodPressure);
<<<<<<< HEAD
		this.sessionFactory.getCurrentSession().saveOrUpdate(bloodPressure.getHealthStatus());
=======
<<<<<<< HEAD
		this.sessionFactory.getCurrentSession().saveOrUpdate(bloodPressure.getHealthStatus());
=======
<<<<<<< HEAD
		this.sessionFactory.getCurrentSession().saveOrUpdate(bloodPressure.getHealthStatus());
=======
		this.sessionFactory.getCurrentSession().saveOrUpdate(healthStatus);
>>>>>>> 08bb5628c58809b26a98454c994d64a267ec564f
>>>>>>> 8aa4961b5a7289c302b67b5aa77f373d5a043285
>>>>>>> 906a8e062214c1fbe50490ea2ce410000de0664d
		this.sessionFactory.getCurrentSession().flush();
		
	}

	@Override
	public BloodPressure getBloodPressureById(long id) {
		// TODO Auto-generated method stub
		Session currentSession  = this.sessionFactory.getCurrentSession();
		BloodPressure bloodPressure = (BloodPressure) currentSession.get(BloodPressure.class, id);
		return bloodPressure;
	}
	
//	@Override
//	public List<BloodPressure> getBloodPressureByEmail(String email) {
//		// TODO Auto-generated method stub
//		Session currentSession  = this.sessionFactory.getCurrentSession();
//		@SuppressWarnings("unchecked")
//		List<BloodPressure> bloodPressureList =currentSession.createSQLQuery(
//				"select * from bloodpressure where email='" +email+"'").list();
//		return bloodPressureList;
//	}	
	@SuppressWarnings("unchecked")
	@Override
	public List<BloodPressure> getBloodPressureByEmail(String email) {
		// TODO Auto-generated method stub
//		Session currentSession  = this.sessionFactory.getCurrentSession();
//		@SuppressWarnings("unchecked")
//		List<BloodPressure> bloodPressureList =currentSession.createQuery(
//				"select id,description,diastolic,systolic,heartRate from "
//				+ "Bloodpressure bp where bp.email=:email").setString("email",email).list();
//		return bloodPressureList;
		
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(BloodPressure.class);
		criteria.add(Restrictions.eq("email", email));
		return criteria.list();

	}	

	
	
	
	@Override
	public void updateBloodPressure(BloodPressure bloodPressure) {
		// TODO Auto-generated method stub
		Session currentSession  = this.sessionFactory.getCurrentSession();
		currentSession.merge(bloodPressure);		
	}

	@Override
	public void deleteBloodPressure(long id) {
		// TODO Auto-generated method stub
		Session currentSession  = this.sessionFactory.getCurrentSession();
		BloodPressure bloodPressure = (BloodPressure) currentSession.get(BloodPressure.class, id);
		currentSession.delete(bloodPressure);
		
	}

}
