package elec5619.group9.service.hypertension;

import java.io.Serializable;
import java.util.List;

import elec5619.group9.domain.hypertension.BloodPressure;
import elec5619.group9.domain.hypertension.HealthStatus;

public interface BloodPressureManager extends Serializable{
	public List<BloodPressure> getBloodPressures();
	public void addBloodPressure(BloodPressure BloodPressure,HealthStatus healthStatus);
	public List<BloodPressure> getBloodPressureByEmail(String email);
	public BloodPressure getBloodPressureById(long id);
	public void updateBloodPressure(BloodPressure bloodPressure);
	public void deleteBloodPressure(long id);

}
