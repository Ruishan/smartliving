package elec5619.group9.web;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import elec5619.group9.domain.User;
import elec5619.group9.service.Register;

@Controller
@RequestMapping(value="/register/**")
@SessionAttributes(value="user",types=User.class)
public class RegisterController {
	
	@Resource(name="register")
	private Register register;
	
	@RequestMapping(value = "/register")
	public String register(Model model) {
		return "register";
	}
	
	@RequestMapping(value="/register", method=RequestMethod.POST)
	public String register(HttpServletRequest httpServletRequest, Model model){
		User user = new User();
		
		String email = httpServletRequest.getParameter("email");
		String firstName = httpServletRequest.getParameter("first_name");
		String lastName = httpServletRequest.getParameter("last_name");
		String password = httpServletRequest.getParameter("password");
		
		if(email == null || email.trim().length() == 0){
			model.addAttribute("email_error", "Please enter email address");
			return "register";
		}
		
		if(!register.isValidEmailFormate(email)){
			model.addAttribute("email_error", "Invalid email formate");
			return "register";
		}
		
		if(firstName == null || firstName.trim().length() == 0 
				|| lastName == null || lastName.trim().length() == 0){
			model.addAttribute("name_error", "Please enter name");
			return "register";
		}
		
		if(password == null || password.trim().length() == 0){
			model.addAttribute("password_error", "Please enter password");
			return "register";
		}
		
		user.setEmail(email);
		user.setIsGoogle(false);
		user.setName(firstName.trim() + " " + lastName.trim()); 
		user.setPassword(password);
		
		
		if(register.isExistUser(user.getEmail())){
			model.addAttribute("email_error", "Email already in the system!");
			return "register";
		}
		
		register.registerUser(user);
		
		model.addAttribute("user", user);
		return "redirect:/HomeSecured";
	}
}
