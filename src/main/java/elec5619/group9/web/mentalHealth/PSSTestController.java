package elec5619.group9.web.mentalHealth;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import elec5619.group9.domain.User;
import elec5619.group9.domain.mentalHealth.PSSResult;
import elec5619.group9.service.mentalHealth.PerformTestService;

@Controller
@SessionAttributes(value="user",types=User.class)
public class PSSTestController {
	
	@Autowired
	private PerformTestService ptService;
	
	@RequestMapping(value="/performTest")
	public String performTest(@ModelAttribute User user, Model model){
		model.addAttribute("user", user);
		return "performTest";
	}
	
	@RequestMapping(value="/performTest", method=RequestMethod.POST)
	public String performTest(@ModelAttribute User user, HttpServletRequest request, Model model){
		model.addAttribute("user", user);
		
		String questionNum = "q";
		boolean completed = false;
		int score = 0;
		
		for(int i = 1; i < 11; i++){
			String selection = request.getParameter(questionNum + Integer.toString(i));
			if (selection != null){
				score += ptService.selectionToScore(selection);
				completed = true;
			}
			else{
				completed = false;
				break;
			}
		}
		
		if(completed == false)
			model.addAttribute("not_complete", "Please complete the question first!");
		else {
			PSSResult result = new PSSResult();
			result.setScore(score);
			if(ptService.addResultFor(user, result)){
				model.addAttribute("score", score);
				model.addAttribute("stress_level", ptService.stressLevel(score));
				model.addAttribute("save_status", "The test result saved!");
			}
			else
				model.addAttribute("save_Status", "Save unseccsefull, try again!");
		}
		return "performTest";
	
	}
	
	@RequestMapping(value="/viewPSSResult")
	public String viewPSSResult(@ModelAttribute User user, Model model){
		model.addAttribute("user", user);
		model.addAttribute("pssresults", user.getResults());
		return "viewPSSResult";
	}
}
