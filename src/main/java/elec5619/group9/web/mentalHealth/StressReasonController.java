package elec5619.group9.web.mentalHealth;

import java.util.Scanner;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import elec5619.group9.domain.User;
import elec5619.group9.domain.mentalHealth.StressReason;
import elec5619.group9.service.mentalHealth.StressReasonService;

@Controller(value="stressReasonController")
@SessionAttributes(value="user",types=User.class)
public class StressReasonController {
	
	@Autowired
	private StressReasonService srService;
	
	@RequestMapping(value="/addStressReason")
	public String addStressReason(@ModelAttribute User user, Model model){
		model.addAttribute("user", user);
		
		return "addStressReason";
	}
	
	@RequestMapping(value="/addStressReason", method=RequestMethod.POST)
	public String addStressReason(@ModelAttribute User user, HttpServletRequest request, Model model){
		model.addAttribute("user", user);
		String categorySelection = request.getParameter("category");
		StressReason reason = new StressReason();
		String input_reason = request.getParameter("reason");
		String input_category = request.getParameter("other_category");
		
		if(input_reason == null || input_reason.trim().length() == 0){
			model.addAttribute("textarea_error", 
					"Please input stress reason to create record");
			return "addStressReason";
		}
		
		if(categorySelection.equalsIgnoreCase("others") && input_category.trim().length() == 0){
			model.addAttribute("other_category_error", 
					"Please specific the category");
			return "addStressReason";
		}
		
		if(!categorySelection.equalsIgnoreCase("others")){
			reason.setCategory(categorySelection);
		} else 
			reason.setCategory(input_category);
		
		reason.setReason(input_reason);
		srService.addStressReason(user, reason);
		
		return "redirect:/viewStressReasons";
	}
	
	@RequestMapping(value="/viewStressReasons")
	public String viewStressReasons(@ModelAttribute User user, Model model){
		model.addAttribute("user", user);
		model.addAttribute("reasons", user.getStressReasons());
		
		return "viewStressReasons";
	}
	

}
