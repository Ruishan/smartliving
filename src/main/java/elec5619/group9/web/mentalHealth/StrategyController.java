package elec5619.group9.web.mentalHealth;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import elec5619.group9.domain.User;
import elec5619.group9.domain.mentalHealth.Strategy;
import elec5619.group9.domain.mentalHealth.StressReason;
import elec5619.group9.service.mentalHealth.StrategyService;

@Controller
public class StrategyController {
	
	@Autowired
	private StrategyService service;
	
	@RequestMapping(value="/commentStrategy", method=RequestMethod.POST)
	public String commentStrategy(@ModelAttribute User user, HttpServletRequest request,
			Model model){
		model.addAttribute("user", user);
		String stressId = request.getParameter("stressId");
		StressReason reason = service.getStressReasonById(Integer.parseInt(stressId));
		model.addAttribute("stress", reason.getReason());
		model.addAttribute("request_stressId", stressId);
		
		return "commentStrategy";
	}
	
	@RequestMapping(value="/addStrategy", method=RequestMethod.POST)
	public String addStrategy(@ModelAttribute User user, HttpServletRequest request,
			Model model){
		model.addAttribute("user", user);
		String stressId = request.getParameter("stressId");
		StressReason reason = service.getStressReasonById(Integer.parseInt(stressId));
		Strategy strategy = new Strategy();
		strategy.setReason(reason.getReason());
		String commentStrategy = request.getParameter("strategy");
		
		if(commentStrategy.trim() != null && commentStrategy.length() > 0){
			commentStrategy = service.restructString(commentStrategy);
			strategy.setStrategy(commentStrategy);
			service.addStrategy(strategy);
			return "redirect:/viewStrategies";
		}else{
			model.addAttribute("textarea_error", "please comment the strategy");
		}
		return "commentStrategy";
	}
	
	@RequestMapping(value="/viewStrategies")
	public String viewStrategies(@ModelAttribute User user, Model model){
		model.addAttribute("user", user);
		ArrayList<Strategy> list = (ArrayList<Strategy>) service.allStrategies();
		model.addAttribute("strategies", list);
		return "viewStrategies";
	}
}
