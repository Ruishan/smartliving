package elec5619.group9.web;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import elec5619.group9.domain.User;



//import elec5619.group9.mentalHealth.service.Utils;



/**
 * Handles requests for the application home page.
 */
@Controller(value="mainPublicController")
public class MainPublicController{
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	
	@RequestMapping(value = "/")
	public String home(@ModelAttribute User user,Model model) {
		
		return "index";
	}
	
	@RequestMapping(value = "/", method=RequestMethod.POST)
	public String redirect(@ModelAttribute User user, HttpServletRequest request,Model model){
		model.addAttribute("user", user);
		return "redirect:index";
	}
	
	
	
}
