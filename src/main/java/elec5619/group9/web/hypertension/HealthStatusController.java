package elec5619.group9.web.hypertension;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

<<<<<<< HEAD
import org.springframework.beans.factory.annotation.Autowired;
=======
<<<<<<< HEAD
import org.springframework.beans.factory.annotation.Autowired;
=======
>>>>>>> 8aa4961b5a7289c302b67b5aa77f373d5a043285
>>>>>>> 906a8e062214c1fbe50490ea2ce410000de0664d
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import elec5619.group9.domain.User;
import elec5619.group9.domain.hypertension.BloodPressure;
import elec5619.group9.domain.mentalHealth.Strategy;
import elec5619.group9.service.hypertension.BloodPressureManager;
<<<<<<< HEAD
import elec5619.group9.service.hypertension.HealthStatusService;
=======
<<<<<<< HEAD
import elec5619.group9.service.hypertension.HealthStatusService;
=======
>>>>>>> 8aa4961b5a7289c302b67b5aa77f373d5a043285
>>>>>>> 906a8e062214c1fbe50490ea2ce410000de0664d

@Controller
@RequestMapping(value="/**")
@SessionAttributes(value="user",types=User.class)
public class HealthStatusController {

	@Resource(name="bloodPressureManager")
	private BloodPressureManager bloodPressureManager;
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 906a8e062214c1fbe50490ea2ce410000de0664d
	
	@Autowired
	private HealthStatusService healthStatusService;
		
<<<<<<< HEAD
=======
=======
   	
>>>>>>> 8aa4961b5a7289c302b67b5aa77f373d5a043285
>>>>>>> 906a8e062214c1fbe50490ea2ce410000de0664d
	
	
	@RequestMapping(value="/viewHealthStatus")
	public String viewHealthStatus(@ModelAttribute User user, Model model){
		model.addAttribute("user", user);
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 906a8e062214c1fbe50490ea2ce410000de0664d
		List<BloodPressure> bl= this.bloodPressureManager.getBloodPressureByEmail(user.getEmail());
		for (BloodPressure b:bl){
			String dias = this.categorizeDiastolic(b.getHeartRate());
			String sys = this.categorizeSystolic(b.getSystolic());
			String hr = this.categorizeHeartRate(b.getHeartRate());

			b.getHealthStatus().setDiastolicStatus(dias);
			b.getHealthStatus().setSystolicStatus(sys);
			b.getHealthStatus().setHeartRateStatus(hr);
			this.healthStatusService.updateHealthStatus(b.getHealthStatus());
			
		}
		model.addAttribute("bloodPressureList", bl);
		return "viewHealthStatus";
	}
	
	private String categorizeSystolic(Integer systolic){
		String[] CATEGORY ={"invalid input","low","normal","high"};
		
		if(systolic <90 && systolic > 0){
			return CATEGORY[1];
		}else if(systolic >= 90 && systolic <=140){
			return CATEGORY[2];
		}else if(systolic > 140){
			return CATEGORY[3];
		}
		
		return CATEGORY[0];
	}
	
	private String categorizeDiastolic(int systolic){
		String[] CATEGORY ={"invalid input","low","normal","high"};
		
		if(systolic <60 && systolic > 0){
			return CATEGORY[1];
		}else if(systolic >= 60 && systolic <=80){
			return CATEGORY[2];
		}else if(systolic > 80){
			return CATEGORY[3];
		}
		
		return CATEGORY[0];
	}
	
	private String categorizeHeartRate(int heartRate){
		String[] CATEGORY ={"invalid input","slow","normal","fast"};
		
		if(heartRate <60 && heartRate > 0){
			return CATEGORY[1];
		}else if(heartRate >= 60 && heartRate <=100){
			return CATEGORY[2];
		}else if(heartRate > 100){
			return CATEGORY[3];
		}
		
		return CATEGORY[0];
	}

<<<<<<< HEAD
=======
=======
		List<BloodPressure> bloodPressureList= this.bloodPressureManager.getBloodPressureByEmail(user.getEmail());
		model.addAttribute("bloodPressureList", bloodPressureList);
		return "viewHealthStatus";
	}
	
	
>>>>>>> 8aa4961b5a7289c302b67b5aa77f373d5a043285
>>>>>>> 906a8e062214c1fbe50490ea2ce410000de0664d
	
	
}