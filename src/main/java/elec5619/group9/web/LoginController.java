package elec5619.group9.web;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import elec5619.group9.domain.User;
import elec5619.group9.service.Login;

@Controller(value="loginController")
@SessionAttributes(value="user",types=User.class)
public class LoginController {
	
	@Resource(name="login")
	private Login login;
	
	@RequestMapping(value = "/login")
	public String login(Model model) {
		return "login";
	}
	
	@RequestMapping(value = "/login", method=RequestMethod.POST)
	public String login(HttpServletRequest httpServletRequest, ModelMap model) {
		User user = new User();
		
		user.setEmail(httpServletRequest.getParameter("email"));
		user.setPassword(httpServletRequest.getParameter("password"));
		
		User validatedUser = login.isValidUser(user);
		if(validatedUser != null){
			model.addAttribute("user", validatedUser);
			return "redirect:/HomeSecured";
		}
		else{
			model.addAttribute("message", "invalid user email and password!");
			return "login";
		}
	}

}
