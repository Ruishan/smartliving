package elec5619.group9.web;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import elec5619.group9.domain.PersonalInformation;
import elec5619.group9.domain.User;
import elec5619.group9.service.PersonalInformationService;

@Controller(value="personalInformationController")
@SessionAttributes(value="user",types=User.class)
public class PersonalInformationController {
	
	@Autowired
	private PersonalInformationService piService;
	
	@RequestMapping(value="/editPersonalInformation")
	public String editPersonalInformation(@ModelAttribute User user, Model model){
		model.addAttribute("user", user);
		
		
		if(user.getPersonalInformation().getGender().equalsIgnoreCase("female"))
			model.addAttribute("female_check", "check");
		else
			model.addAttribute("male_check", "check");
		
		if(user.getPersonalInformation().getMarriage() == false)
			model.addAttribute("marriage_no", "no");
		else
			model.addAttribute("marriage_yes", "yes");
			
		
		
		return "editPersonalInformation";
	}
	
	@RequestMapping(value="/editPersonalInformation", method=RequestMethod.POST)
	public String editPersoanlInformation(@ModelAttribute User user, HttpServletRequest request, Model model){
		model.addAttribute("user", user);
		String temp = request.getParameter("age");
		int age = piService.isvalidAge(temp);
		String gender = request.getParameter("gender");
		String marriage = request.getParameter("marriage");
		String occupation = piService.isValidOccupation(request.getParameter("occupation"));
		
		
		
		if(age >= 0 && !occupation.equals("0")){
			
			PersonalInformation pi = user.getPersonalInformation();
			pi.setAge(age);
			pi.setGender(gender);
			pi.setMarriage(piService.marriageToBoolean(marriage));
			pi.setOccupation(occupation); 
			
			if (piService.savePersonalInformationFor(user, pi))
				model.addAttribute("save_successful", "saved!");
		}
		else{
			model.addAttribute("invalid_age", "Invalid age, please try again");
			if(occupation.equals("0"))
			model.addAttribute("invalid_occupation", "Invalid occupation, please try again");
		}
		
		
		return "editPersonalInformation"; 
	}
}
