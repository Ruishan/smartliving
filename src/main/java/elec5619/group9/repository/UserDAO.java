package elec5619.group9.repository;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.SessionFactory;

import elec5619.group9.domain.PersonalInformation;
import elec5619.group9.domain.User;
import elec5619.group9.domain.mentalHealth.PSSResult;
import elec5619.group9.domain.mentalHealth.Strategy;
import elec5619.group9.domain.mentalHealth.StressReason;

public interface UserDAO {
	public void addUser(User user);
	public User getUserByEmailId(String email);
	public void savePersonalInformation(User user, PersonalInformation pi);
	public void addPSSResultFor(User user, PSSResult result);
	public void addStressReasonFor(User user, StressReason reason);
	public SessionFactory getSessionFactory();
}
