package elec5619.group9.repository.hypertension;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import elec5619.group9.domain.hypertension.HealthStatus;


@Repository
@Transactional
public class HealthStatusDAOImp implements HealthStatusDAO{

	private SessionFactory sessionFactory;
	
	
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory){
		this.sessionFactory = sessionFactory;
	}
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	
	private Session currentSession(){
		return sessionFactory.getCurrentSession();
	}	
	
	

	@Override
	public void updateHealthStatus(HealthStatus healthStatus) {
		// TODO Auto-generated method stub
		Session currentSession  = this.sessionFactory.getCurrentSession();
<<<<<<< HEAD
		currentSession.update(healthStatus);		
=======
<<<<<<< HEAD
		currentSession.update(healthStatus);		
=======
		currentSession.merge(healthStatus);		
>>>>>>> 8aa4961b5a7289c302b67b5aa77f373d5a043285
>>>>>>> 906a8e062214c1fbe50490ea2ce410000de0664d
	}

}
