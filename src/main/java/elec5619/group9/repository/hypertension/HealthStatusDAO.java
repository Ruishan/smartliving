package elec5619.group9.repository.hypertension;

import elec5619.group9.domain.hypertension.HealthStatus;

public interface HealthStatusDAO {
	public void updateHealthStatus(HealthStatus healthStatus);

}
