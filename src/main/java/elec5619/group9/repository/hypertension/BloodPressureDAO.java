package elec5619.group9.repository.hypertension;

import java.util.List;


import elec5619.group9.domain.hypertension.BloodPressure;

public interface BloodPressureDAO {
	public void addBloodPressure(BloodPressure bloodPressure);
	public void updateBloodPressure(BloodPressure bloodPressure);
	public void deleteBloodPressure(BloodPressure bloodPressure);
	public List<BloodPressure> getBloodPressureByEmail(String email);

}
