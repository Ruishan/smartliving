package elec5619.group9.repository.mentalHealth;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import elec5619.group9.domain.mentalHealth.Strategy;
import elec5619.group9.domain.mentalHealth.StressReason;

@Repository
@Transactional
public class MenatlHealthDAOImp implements MenatalHealthDAO{

	
	private SessionFactory sessionFactory;
	
	@Override
	public Strategy getStrategyById(int id) {
		return (Strategy) currentSession().get(Strategy.class, id);
	}
	
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory){
		this.sessionFactory = sessionFactory;
	}
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	
	private Session currentSession(){
		return sessionFactory.getCurrentSession();
	}

	@Override
	public StressReason getStressReasonById(int id) {
		return (StressReason) currentSession().get(StressReason.class, id);
	}

	@Override
	public void addStratey(Strategy strategy) {
		currentSession().save(strategy);
		currentSession().flush();
		
	}

	@Override
	public List<Strategy> allStrateties() {
		return currentSession().createCriteria(Strategy.class).list();
	}

}
