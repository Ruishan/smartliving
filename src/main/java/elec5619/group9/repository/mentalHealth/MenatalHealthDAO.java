package elec5619.group9.repository.mentalHealth;

import java.util.List;

import elec5619.group9.domain.mentalHealth.Strategy;
import elec5619.group9.domain.mentalHealth.StressReason;

public interface MenatalHealthDAO {
	public Strategy getStrategyById(int id);
	public StressReason getStressReasonById(int id);
	public void addStratey(Strategy strategy);
	public List<Strategy> allStrateties();
}
