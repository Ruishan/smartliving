package elec5619.group9.repository.mentalHealth;

import elec5619.group9.domain.mentalHealth.Strategy;

public interface StrategyDAO {
	public Strategy getStrategyById(int id);
}
