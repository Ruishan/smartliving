package elec5619.group9.repository;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import elec5619.group9.domain.PersonalInformation;
import elec5619.group9.domain.User;
import elec5619.group9.domain.mentalHealth.PSSResult;
import elec5619.group9.domain.mentalHealth.Strategy;
import elec5619.group9.domain.mentalHealth.StressReason;
@Repository
@Transactional
public class UserDAOImp implements UserDAO{
	
	private SessionFactory sessionFactory;
	
	@Override
	public void addUser(User u) {
		currentSession().save(u);
		currentSession().saveOrUpdate(u.getPersonalInformation());
		currentSession().flush();
		String pass = "";
//		try {
//			pass = sha256(u.getPassword());
//		} catch (NoSuchAlgorithmException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (UnsupportedEncodingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		SQLQuery query = currentSession().createSQLQuery("INSERT INTO smartliving.smart_user VALUES (NULL , 1, '" + pass + "', '" + u.getEmail() + "', 0)");
//		query.executeUpdate();
	}
	
//    private String sha256(String password) throws NoSuchAlgorithmException, UnsupportedEncodingException {
//    	MessageDigest digest = MessageDigest.getInstance("SHA-256");
//    	digest.update(password.getBytes("UTF-8"));
//    	byte[] hash = digest.digest();
//    	StringBuffer sb = new StringBuffer();
//    	for (int i = 0; i < hash.length; i++) {
//    		sb.append(Integer.toString((hash[i] & 0xff) + 0x100, 16).substring(1));
//    	}
//    	return sb.toString();
//    }
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory){
		this.sessionFactory = sessionFactory;
	}

	@Override
	public User getUserByEmailId(String email) {
		return (User)currentSession().get(User.class, email);
	}
	
	public void savePersonalInformation(User user, PersonalInformation pi){
		pi.setEmail(user.getEmail());
		currentSession().saveOrUpdate(pi);
		user.setPersonalInformation(pi);
		currentSession().update(user);
		currentSession().flush();
	}
	
	private Session currentSession(){
		return sessionFactory.getCurrentSession();
	}

	@Override
	public void addPSSResultFor(User user, PSSResult result) {
	//	result.setEmail(user.getEmail());
		result.setUser(user);
		result.setTime(new Date());
		currentSession().save(result);
		user.getResults().add(result);
		currentSession().saveOrUpdate(user);
		currentSession().flush();
	}

	
	@Override
	public void addStressReasonFor(User user, StressReason reason) {
		currentSession().save(reason);
		user.getStressReasons().add(reason);
		currentSession().saveOrUpdate(user);
		currentSession().flush();
		
	}

}
